import JsonApi from 'devour-client'


const api_url_root = 'http://127.0.0.1:8000';


var rss_data = new JsonApi({apiUrl:api_url_root})

rss_data.define('category', {
    slug: "",
    title: "",
    articles: ""
})


rss_data.define('article', {
    title: "",
    external_id: "",
    link: "", 
    image_url: "",
    description: "",
    date: "",
    category: {
        jsonApi: 'hasMany',
        type: 'category'
      }
})


export const state = () => ({
    categories: {
        links: {},
        data: [],
        meta: {
            pagination: {}
        }
    },
    articles: {},
    pagination: {},
});

export const mutations = {
    FETCH_CATEGORIES(state, fetched_data) {
        state.categories = fetched_data;
    },
    FETCH_ARTICLES(state, fetched_data) {
        state.articles = fetched_data;
        state.pagination = fetched_data.meta.pagination
    },
};

export const actions = {
    fetchCategories({commit})  {
        rss_data.findAll('category').then((res) => {
            commit("FETCH_CATEGORIES", res);
        })
    },
    fetchArticles({commit}, page)  {
        if(page) {
            rss_data.findAll('article', {page: page}).then((res) => {
                commit("FETCH_ARTICLES", res);
            })
        }
        else {
            rss_data.findAll('article').then((res) => {
                commit("FETCH_ARTICLES", res);
            })
        }
    },

    fetchCategoryArticles({commit}, data)  { 
        rss_data.request(api_url_root+"/categories/"+data.slug+"/", 'GET', {page: data.page}).then((res) => {
            commit("FETCH_ARTICLES", res);
        })
    },

    fetchArticlesFromSearch({commit}, q) {
        rss_data.request(api_url_root+"/categories/fun/").then((res) => {
            commit("FETCH_ARTICLES", res);
        })
    }
}



export const getters = {
    activeCategory: (state) => {
        return "TEst"
    }
}

