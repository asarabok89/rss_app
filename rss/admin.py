from django.contrib import admin
from .models import Article, Category, ArticlesImport

class ArticleAdmin(admin.ModelAdmin):
	list_display = ('id', 'external_id', 'title', 'date')

class CategoryAdmin(admin.ModelAdmin):
	list_display = ('id', 'title', 'url_name')

class ImportAdmin(admin.ModelAdmin):
	list_display = ('id', 'category', 'feed_build_date', 'date_imported')

admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ArticlesImport, ImportAdmin)
