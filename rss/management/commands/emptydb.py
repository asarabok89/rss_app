from django.core.management.base import BaseCommand
from rss.models import Article, ArticlesImport, Category
from backend.settings import RSS_CHANNELS

class Command(BaseCommand):
    def handle(self, *args, **options):
        Article.objects.all().delete()
        ArticlesImport.objects.all().delete()
        Category.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('DB data deleted'))

