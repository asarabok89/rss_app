from django.core.management.base import BaseCommand, CommandError
from rss.models import Article, ArticlesImport
from django.db import connection

class Command(BaseCommand):
    help = 'Delete all articles'
    
    def handle(self, *args, **options):
        Article.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('Articles data deleted'))   

        ArticlesImport.objects.all().delete()
        self.stdout.write(self.style.SUCCESS('ArticlesImport data deleted')) 
