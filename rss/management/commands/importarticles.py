# -*- coding: utf-8 -*-

import urllib.request
import xml.etree.ElementTree as ET
from datetime import datetime
from html.parser import HTMLParser

import feedparser
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

import dateutil.parser
from rss.models import Article, ArticlesImport, Category


class Command(BaseCommand):
    help = 'Save News, Show, Sport and Tech to DB'

    def handle(self, *args, **options):
        categories = Category.objects.all()
        for category in categories:
            url = 'https://www.24sata.hr/feeds/'+category.url_name+'.xml'
            self.stdout.write('\nREADING '+url)
            feed = feedparser.parse(url)
            feed_build_date = dateutil.parser.parse(feed.feed.date)
            #feed_build_date = datetime.strptime(feed.feed.date, "%a, %d %b %Y %H:%M:%S %z")
            #feed_build_date = feed_build_date.strftime("%Y-%m-%d %H:%M:%S")

            #check feed_build_date from existing items in DB
            if ArticlesImport.objects.filter(category=category, feed_build_date=feed_build_date):
                self.stdout.write(self.style.WARNING(f"ALREADY IMPORTED ARTICLES WITH CATEGORY {category.title} AND IMPORT DATE {feed_build_date.strftime('%d.%m.%Y %H:%M')}"))
            else:
                # imports from rss
                articles_import = ArticlesImport(category=category, feed_build_date=feed_build_date)
                articles_import.save()

                for article in feed['items']:
                    external_id = article["guid"][-6:]
                    if not self.isArticleImported(external_id):

                        description_node = article["description"]
                        html_parser = ParseHtml()
                        html_parser.feed(description_node)
                        img_url = html_parser.get_img_src()
                        description = html_parser.get_description()

                        new_article = Article(\
                                    external_id=external_id, \
                                    title=article["title"], \
                                    category=category, \
                                    link=article["link"], \
                                    image_url=img_url, \
                                    description=description, \
                                    date=dateutil.parser.parse(article["published"]), \
                                    articles_import=articles_import)
                        new_article.save()

                self.stdout.write(self.style.SUCCESS('Import OK'))

    def isArticleImported(self, article_external_id):
        return bool(Article.objects.filter(external_id=article_external_id))

class ParseHtml(HTMLParser):

    def handle_starttag(self, tag, attrs):
        self.img = attrs[0][1]

    def handle_data(self, data):
        self.description = data

    def get_img_src(self):
        return self.img

    def get_description(self):
        return self.description
