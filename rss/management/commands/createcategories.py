from django.core.management.base import BaseCommand
from rss.models import Category
from backend.settings import RSS_CHANNELS

class Command(BaseCommand):
    def handle(self, *args, **options):
        Category.objects.all().delete()
        for rss_channel in RSS_CHANNELS:
            Category.objects.create(title=rss_channel.get('title'), url_name=rss_channel.get('url_name'))
        self.stdout.write(self.style.SUCCESS('Categories created'))   
        
        
