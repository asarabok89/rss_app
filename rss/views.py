from rest_framework import (filters, generics, mixins, permissions, renderers,
                            viewsets)
from rest_framework.response import Response

from rss.models import Article, ArticlesImport, Category
from rss.serializers import (ArticleSerializer, CategoryArticleSerializer,
                             CategorySerializer)


class CategoriesView(generics.ListAPIView):
    serializer_class = CategorySerializer

    def get_queryset(self):
        return Category.objects.all()


class CategoryArticlesView(generics.ListAPIView):
    serializer_class = CategoryArticleSerializer

    def get_queryset(self):
        self.category = Category.objects.get(**self.kwargs)
        return Article.objects.filter(category=self.category).order_by('-date')

    def list(self, request, *args, **kwargs):
        response = super().list(request, args, kwargs)
        response.data['category'] = self.category.title
        return response


class ArticlesView(generics.ListAPIView):
    queryset = Article.objects.all().order_by('-date')
    serializer_class = ArticleSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['title', 'description']
