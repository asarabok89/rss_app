from django.urls import path
from django.conf.urls import include, url
from django.views.generic import RedirectView
from rest_framework.routers import DefaultRouter

from .views import CategoryArticlesView, CategoriesView, ArticlesView

app_name = "rss"

urlpatterns = [
    path('articles/', ArticlesView.as_view()),
    path("category/", CategoriesView.as_view()),
    path(
        "category/<str:url_name>/",
        CategoryArticlesView.as_view(),
        name="category-articles",
    ),
]
