from django.urls import reverse
from rest_framework import serializers

from rss.models import Article, ArticlesImport, Category


class CategorySerializer(serializers.ModelSerializer):
    articles = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ("id", "title", "url_name", "articles")

    def get_articles(self, obj):
        request = self.context.get("request")
        return request.build_absolute_uri(
            reverse("rss:category-articles", kwargs={"url_name": obj.url_name})
        )


class CategoryArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = (
            "title",
            "external_id",
            "link",
            "image_url",
            "description",
            "date"
        )


class ArticleSerializer(CategoryArticleSerializer):
    category = CategorySerializer()

    class Meta(CategoryArticleSerializer.Meta):
        fields = CategoryArticleSerializer.Meta.fields + ('category', )
