from datetime import datetime

class ArticleDateTime:
	def datetime_string_to_croatian_form(str):
		datetime_object = datetime.strptime(str, '%a, %d %b %Y %H:%M:%S %z')
		croatian_datetime = datetime_object.strftime("%d.%m.%Y %H:%M:%S")
		return croatian_datetime
	def datetime_string_to_timestamp(str):
		datetime_object = datetime.strptime(str, '%a, %d %b %Y %H:%M:%S %z')
		return datetime_object.timestamp()	


