from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=10)
    url_name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.title


class ArticlesImport(models.Model):
    category = models.ForeignKey(
        Category, related_name="articles_import", on_delete=models.CASCADE
    )
    feed_build_date = models.DateTimeField()
    date_imported = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.date_imported.strftime("%Y-%m-%d %H:%M")


class Article(models.Model):
    external_id = models.IntegerField()
    title = models.CharField(max_length=300)
    category = models.ForeignKey(
        Category,
        null=True,
        blank=True,
        default=None,
        related_name="articles",
        on_delete=models.CASCADE,
    )
    link = models.CharField(max_length=300)
    image_url = models.CharField(max_length=300)
    description = models.TextField()
    date = models.DateTimeField()
    articles_import = models.ForeignKey(
        ArticlesImport, related_name="articles", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.title
