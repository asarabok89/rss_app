from django.contrib import admin
from django.urls import include, path
from rss.models import Category, ArticlesImport, Article



urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('rss.urls')),
]
