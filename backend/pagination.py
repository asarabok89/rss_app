from rest_framework_json_api.pagination import JsonApiPageNumberPagination

class RssPagePagination(JsonApiPageNumberPagination):
    page_query_param = 'page'
    page_size_query_param = 'page_size'
