# ABOUT
RSS_APP is a Django/AngularJS app that reads RSS feed from 24sata media portal. Data is saved in SQLite database.
# REQUIREMENTS:
- NodeJS 8.X+ installed https://nodejs.org/en/
- Python 3.x + installed https://www.python.org/
- Django 2.1.7. installed `pip install Django==2.1.7`
- feedparser for parsing feeds `pip install feedparser`
- django-cors-headers for adding CORS headers `pip install django-cors-headers`
# QUICKSTART
1. Install packages from url above
2. Clone this repository
3. Open two CMD windows and enter into app directory `cd your_path/to/rss_app`
    1. In first CMD window start django webserver `python manage.py runserver`. It runs on `localhost:8000` and serves JSON data.
    2. In second CMD window enter into public directory `cd public` and type `npm install`. After installation of npm packages run `npm start` to start the AngularJS webserver. It runs on `localhost:3000` and serves frontend app. Open your favorite web browser and enter `localhost:3000` in url bar 

# DJANGO API ENDPOINTS
- `http://localhost:8000/rss/all_channels` JSON data of added channels
- `http://localhost:8000/rss/articles` JSON data of all articles
- `http://localhost:8000/rss/articles/page/{n}` JSON data of all articles with pagination where {n} is page number.
- `http://localhost:8000/rss/articles/category/{category_name}` JSON data of articles from selected category. {category_name} can have values `news`, `show`,`sport` and `tech`.
- `http://localhost:8000/rss/articles/category/{category_name}/page/{n}` JSON data of articles with pagination from selected category. {category_name} can have values `news`, `show`,`sport` and `tech`.
- `http://localhost:8000/rss/articles/search?q={search_string}` JSON data of articles with search query {search_string}


# EXTRAS
- For Django admin panel open `localhost:8000/admin` and log in with data antonio/antonio
- Import articles from RSS feed with command `python manage.py importarticles`
- Delete articles `python manage.py deletearticles`